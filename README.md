# Multi-task Learning for the Joint Recognition of User Identity and Affective States Using Physiological Signals

## High-level Methodology

![Overview of the MTL pipeline](assets/MTL_pipeline.png)

The goal of the Affect Recognition (AR) task is to differentiate between Stress and No-stress situations, whilst the User Recognition (UR) task is exploited to recognize the user identity.
The Multi-task Learning (MTL) approach has been developed to exploit the commonalities and differences across these two tasks in order to further improve the performance of the two, jointly.

The multi-task learning model presented contains two distinct networks. There is a specific network for affect recognition and another one for user recognition with their respective tasks. The important piece of the network that deserves a special mention is the fact that there is interaction between the two, which enables to share knowledge.

In the Multi-task methodology, the backbone of the network architecture are the input layers which are fed with the dataset input shown in this repository.
The AR branch uses LOSO as experimental setup, whilst the UR branch uses the Start/End Split methodology.

## Repository Structure

The model implementation is in the jupyter notebook file `MTL_model.ipynb`.  
Inside the `dataset` folder, the archive containing the required dataset csv files are present.  
The graphical overview of the model can be found in the `assets` folder.  
The naming convention used for the dataset-related files is the following:

* **Start/End** Split
    * Start minutes --> `{window_size}_sec_window_data_first_{split_variant}_min`
    * End minutes --> `{window_size}_sec_window_data_last`
* **LOSO** Experimental Setup
    * Stress/No-Stress binary scenario --> `Stress_No_Stress_dataset_{window_size}_sec_window`